<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name', 'email', 'password','is_active', 'age', 'phone', 'first_time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUser($email){
        $user= User::where('email', $email)->first();
        return $user;
    }

    public function getUserById($id){
        $user= User::where('id', $id)->first();
        return $user;
    }

    public function activeUser($email){
        $result = User::where('email',$email)->update(['is_active'=> 1]);
        return $result;
    }

    public function updatePassword($id, $password){
        $result = User::where('id',$id)->update(['password'=> $password]);
        return $result;
    }

    public function updateFirstTime($id){
        $result = User::where('id', $id)->update(['first_time'=> 1]);
        return $result;
    }

    public function editUserData($arrUser, $id){
        $result = User::where('id', $id)->update($arrUser);
        return $result;
    }

    public function getNormalUsers(){
        $arrUsersEmail = array();
        $normalUsers = User::where('role_id', 2)->get();
        foreach($normalUsers as $key => $value){
            $arrUsersEmail[$key] = array(
                'email' =>  $value['email'],
                'id'    =>  $value['id']
            );
        }
        return $arrUsersEmail;
    }

}
