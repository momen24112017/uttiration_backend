<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromocodeTransaction extends Model
{
    protected $fillable = [
        'promocode_id', 'user_id'
    ];
}
