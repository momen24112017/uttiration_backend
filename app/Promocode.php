<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    protected $fillable = [
        'promocode', 'value', 'valid_date'
    ];

    public function checkPromocode($promocode){
        $promocodeExist = Promocode::where('promocode', $promocode)->first();
        if($promocodeExist){
            $validDate = Promocode::where('promocode', $promocode)->get('valid_date');
            return $validDate;
        }
        else{
            return false;
        }
    }

    public function promocodeValue($promocode){
        $value = Promocode::where('promocode', $promocode)->get('value');
        return $value;
    }
    public function promocode($promocode){
        $value = Promocode::where('promocode', $promocode)->first();
        return $value;
    }
    public function getPromocodebyId($promo_id){
        $promocode = $this->where('id',$promo_id)->first();
        return $promocode;
    }

}
