<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQuestionnaire extends Model
{
    protected $fillable = [
        'user_id','user_questionnaire', 'file'
    ];

    public function addFile($path){
        $update = UserQuestionnaire::where('file', null)->orderBy('id', 'DESC')->update(['file'=> $path]);
        return $update;
    }

    public function getUserQuestionnaire($user_id){
        $userQuestionnaire = UserQuestionnaire::where('user_id', $user_id)->first();
        $userProfile = json_decode($userQuestionnaire['user_questionnaire']);
        // dd($userProfile);
        $UserData = array();
        foreach($userProfile as $key => $value){
            // dd($value->q);
            if($value->q == 'medicalCondition' || $value->q == 'foodAllergy'){
                $UserData[] = (array)$value;
            }
        }
        // dd($UserData);
        return $UserData;
    }

}
