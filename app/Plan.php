<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'name', 'price', 'short_desc', 'long_desc', 'banner_img', 'feature_img'
    ];

    public function listPlane(){
        $listPlane = Plan::all();
        foreach($listPlane as $image=>$value){
            $value['banner_img'] = env('App_Media_URL').$value['banner_img'];
            $value['feature_img'] = env('App_Media_URL').$value['feature_img'];
            $value['menu_sample'] = env('App_Media_URL').$value['menu_sample'];
            if($value['paln_table'] != null){
                $value['paln_table'] = env('App_Media_URL').$value['paln_table'];
            }
            //$string = strtolower($string);
            $arrFeature = explode(",",$value['feature']);
            $string = str_replace("\n", "", $value['long_feature']);
            $string = str_replace("\r", "", $string);
            $string = rtrim($string,".");
            $arrLongFeature = explode(".",$string );
            $value['feature'] = $arrFeature;
            $value['long_feature'] = $arrLongFeature;
        }
        
        return $listPlane;
    }

    public function getPlaneById($id){
        $plan= Plan::where('id', $id)->first();
        $string = str_replace(" ", "", $plan->feature);
            $string = strtolower($string);
            $arrFeature = explode(",",$string);
            $plan['feature'] = $arrFeature;
        return $plan;
    }

    public function menu_plans(){
        return $this->hasMany('App\MenuPlan');
    }

}
