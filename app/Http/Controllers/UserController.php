<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

use App\Status;
use App\User;
use App\FreezePlan;
use App\AddressUser;
use App\PlanTransaction;
use App\UserQuestionnaire;
use App\MenuPlan;
use App\MenuUser;
use App\Promocode;

use App\Helpers\mailerHelper;
use App\Mail\ConfirmEmail;
use App\Mail\ContactUs;
use App\Mail\MealsSelected;
use App\Mail\ForgotPassword;
use App\Mail\UserFreezePlan;
use App\Mail\ReportAdminPlanFreeze;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExportUserQuestionnaire;
use App\Exports\ExportSubmitMenu;

class UserController extends Controller
{
    public function Register(Request $request){
        
        $arr= array();
        $input=$request->all();
        
        //to make all inputs required
        if($input['email'] == null || $input['password'] == null ||
            $input['name'] == null || $input['age'] == null ||
            $input['phone'] == null || $input['address'] == null){

            $arr['data'] = $input;
            $arr = Status::mergeStatus($arr,5018);
            return $arr;
        }
        
        //check user exist
        $objUser = new User();
        $user = $objUser->getUser($input['email']);
        if($user != null){
            $arr['data'] = $input;
            $arr = Status::mergeStatus($arr,4016);
            return $arr;
        }
        
        //hash password & make hashed password input
        $pass=$input['password'];
        $password = Hash::make($pass);
        $input['password']= $password;
        $input['avatar'] = "users/August2020/TYEqVnqPPWXGYMPWAXBD.png";
       
        //store user data
        $result = User::create($input);
         
        //store user address
        $addressInput = array(
            'user_id'     => $result['id'],
            'address_name' => "Home",
            'address'     => $input['address'],
        );
        $userAddress = AddressUser::create($addressInput);

        //send mail to active user
        $data = array(
            'name'    => $input['name'],
            'email'   => $input['email'],
        ); 
        $resultEmail = Mail::to($data['email'])->send(new ConfirmEmail($data));
        
        if($result){
            $arr['data'] = $input;
            $arr['user_id'] = $result['id'];
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }
    }

    public function ResendMail($user_id){
        $objUser = new User();
        $user = $objUser->getUserById($user_id);
        $data = array(
            'name'    => $user['name'],
            'email'   => $user['email'],
        ); 
        $resultEmail = Mail::to($data['email'])->send(new ConfirmEmail($data));
        $arr['data'] = $data;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function Login(Request $request){
        
        $arr= array();
        $email = $request->email;
        $password = $request->password;

        //get user info by email
        $objUser = new User();
        $user = $objUser->getUser($email);
        
        //check if user exist
        if($user != null){
            
            //check password
            if (Hash::check($password, $user['password'])){   
                $arr['data']= $user;
                $arr = Status::mergeStatus($arr,200);
                return $arr;
            }
            $arr['data']['email'] = $email;
            $arr['data']['password'] = $password;
            $arr = Status::mergeStatus($arr,5013);
            return $arr;
        }
        else{
            $arr['data'] = $user;
            $arr = Status::mergeStatus($arr,4030);
            return $arr;
        }
    }

    public function ActiveUser($email){
        $arr = array();
        $objUser = new User();
        $user = $objUser->activeUser($email);
        $arr['data'] = $user;
        return redirect('https://uttrition-utrition.azurewebsites.net/success_verify.php');
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ForgotPassword($id){
        return redirect('https://uttrition-utrition.azurewebsites.net/renew_password.php?id='.$id);
    }
    
    public function ResendMailforResetPassword($email){
        $arr = array();
        $objUser = new User();
        $user = $objUser->getUser($email);
        $data = array(
            'id'      => $user['id'],
            'name'    => $user['name'],
            'email'   => $user['email'],
        );
        $resultEmail = Mail::to($data['email'])->send(new ForgotPassword($data));
        $arr['data'] =  $user['email'];
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function CheckEmail($email){
        $arr = array();
        $objUser = new User();
        $user = $objUser->getUser($email);
        if ($user){
            $data = array(
                'id'      => $user['id'],
                'name'    => $user['name'],
                'email'   => $user['email'],
            );
            $resultEmail = Mail::to($data['email'])->send(new ForgotPassword($data));
            $arr['data'] =  $user['email'];
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }
        $arr = Status::mergeStatus($arr,5017);
        return $arr;
    }

    public function ResetPassword(Request $request, $email){
        $arr = array();
        $objUser = new User();
        $input = $request->all();
        $user = $objUser->getUser($email);
        if($input['password'] == null || $input['password_c'] == null){
            $arr['password'] = $input['password'];
            $arr['password_c'] = $input['password_c'];
            $arr = Status::mergeStatus($arr,5018);
            return $arr;
        }
        if($input['password'] != $input['password_c']){
            $arr['password'] = $input['password'];
            $arr['password_c'] = $input['password_c'];
            $arr = Status::mergeStatus($arr,5013);
            return $arr;
        }
        $password = Hash::make($input['password']);
        $result = $objUser->updatePassword($user['id'], $password);
        $arr['data'] = $input['password'];
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function UpdateFirstTime($id){
        $arr = array();
        $objUser = new User();
        $result = $objUser->updateFirstTime($id);
        $arr['data'] = $result;
        // return redirect('http://localhost/uttirtion/index.php');
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function GetUserData($user_id){
        $arr = array();
        $objUser = new User();
        $User = $objUser->getUserById($user_id);
        $objAddress = new AddressUser();
        $arrUserAddress = $objAddress->getAddressAttachedtoUser($user_id);
        $objUserQuestionnaire = new UserQuestionnaire();
        $resultQuestionnaire = $objUserQuestionnaire->getUserQuestionnaire($user_id); 
        $objFreezePlan = new FreezePlan;
        $resultFreezePlan = $objFreezePlan->checkFreezeplan($user_id);
        $objPlanTrans = new PlanTransaction();
        $arrPlanTrans = $objPlanTrans->getUserPlanTrans($user_id);
        $objMenuUser = new MenuUser();
        $arrMenuandSubmitPlan = $objMenuUser->getMenuPlanAttachedtoPlanTrans($arrPlanTrans);
        $arr['user'] = $User;
        $arr['freeze_plan'] = $resultFreezePlan;
        $arr['user_address'] = $arrUserAddress;
        $arr['plan_ass'] = $arrMenuandSubmitPlan;
        $arr['user_q'] = $resultQuestionnaire;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function EditUserData(Request $request, $id){
        $arr = array();
        $input = $request->all();
        $arrUser = array(
            'name'      => $input['name'],
            'age'       => $input['age'],
            'phone'     => $input['phone']
        );

        if(isset($input['password'])){
            $pass=$input['password'];
            $password = Hash::make($pass);
            $input['password'] = $password;
            $arrUser['password'] = $input['password'];
        }

        $objUser = new User();
        $User = $objUser->editUserData($arrUser, $id);
        $objAddress = new AddressUser();
        $UserAddress = $objAddress->editUserAddress($input['arrAddress'], $id);

        $arr['data'][] = $arrUser;
        $arr['data'][] = $UserAddress;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function StoreUserPlan(Request $request, $id){
        $arr = array();
        $input = $request->all();
        $objPlan = new PlanTransaction();
        $result = $objPlan->storeUserPlan($input, $id);
        
        $objUser = new User();
        $user=$objUser->getUserById($id);
        $userEmail = $user['email'];
        $data = array(
            'name'    => $user['name'],
            'email'   => $user['email'],
        ); 
        // $resultEmail = Mail::to($data['email'])->send(new MealsSelected($data));
        
        $exportUser = Excel::store(new ExportSubmitMenu, 'public/'.$id.$userEmail.'.xlsx');
        $path = "$id$userEmail.xlsx";
        $file = $objPlan->addSubmitMenu($path, $id);

        $arr['data'] = $result;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function UserQuestionnaire(Request $request, $id){
        $arr = array();
        $arrInput = $request->all();
        $i = 0;
        foreach($arrInput as $key => $value ){
            $arrExcel[$i] = array(
                'q' => $key,
                'a' => $value,
            );
            $i++;
        }
        // dd($arrExcel);
        $userQuestionnaire = json_encode($arrExcel);
        $input = array(
            'user_id'            => $id,
            'user_questionnaire' => $userQuestionnaire,
        );
        $result = UserQuestionnaire::create($input);
        $objUser = new User();
        $user=$objUser->getUserById($id);
        $userName = $user['name'];
        $exportUser = Excel::store(new ExportUserQuestionnaire, 'public/'.$id.$userName.'.xlsx');
        $path = "$id$userName.xlsx";
        $objUserQuestionnaire = new UserQuestionnaire();
        $file = $objUserQuestionnaire->addFile($path);
        $resultUpdateFirstTime = $objUser->updateFirstTime($id);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ContactUs(Request $request){
        $arr = array();
        $input = $request->all();
        $data = array(
            'name'          => $input['first_name'],
            'last_name'     => $input['last_name'],
            'email'         => $input['email'],
            'phone'         => $input['phone'],
            'message'       => $input['message'],
        ); 
        $resultEmail = Mail::to("banin.chahine@utritionlife.com")->send(new ContactUs($data));
        $arr['data'] = $data;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function FreezePlan(Request $request){
        $arr = array();
        $input = $request->all();
        $today = now();
        $from = date('Y-m-d', strtotime($today . " +2 days"));
        if($input['from'] < $from){
            $arr['data'] = $input['from'];
            $arr = Status::mergeStatus($arr,5019);
            return $arr;
        }
        $to = date('Y-m-d', strtotime($input['from'] . " +45 days"));
        if($input['to'] > $to){
            $arr['data'] = $input['to'];
            $arr = Status::mergeStatus($arr,5019);
            return $arr;
        }
        $result = FreezePlan::create($input);
        
        $objUser = new User();
        $user=$objUser->getUserById($input['user_id']);
        $data = array(
            'name'  => $user['name'],
            'email' => $user['email'],
            'from'  => $input['from'],
            'to'    => $input['to'],
        ); 
        $resultEmailUser = Mail::to($data['email'])->send(new UserFreezePlan($data));
        $resultEmailAdmin = Mail::to('mirette.mahmoud@rethinktechs.com')->send(new ReportAdminPlanFreeze($data));
        
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ShowUserProfile(){
        $arr = array();
        $objUser = new User();
        $users = $objUser->getNormalUsers();
        $arr['data'] = $users;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ViewProfile($user_id){
        return redirect('https://uttrition-utrition.azurewebsites.net/admin-preview_user_profile.php?id='.$user_id);
    }

    public function ViewUserProfile(){
        $objUser = new User();
        $users = $objUser->getNormalUsers();
        return view('users_profiles', compact('users'));
    }

    public function UserProfile($user_id){
        $arr = array();
        $objUser = new User();
        $User = $objUser->getUserById($user_id);
        $objAddress = new AddressUser();
        $arrUserAddress = $objAddress->getAddressAttachedtoUser($user_id);
        $objUserQuestionnaire = new UserQuestionnaire();
        $resultQuestionnaire = $objUserQuestionnaire->getUserQuestionnaire($user_id); 
        $objFreezePlan = new FreezePlan;
        $resultFreezePlan = $objFreezePlan->checkFreezeplan($user_id);
        $objPlanTrans = new PlanTransaction();
        $arrPlanTrans = $objPlanTrans->getUserPlanTrans($user_id);
        $objMenuUser = new MenuUser();
        $arrMenuandSubmitPlan = $objMenuUser->getMenuPlanAttachedtoPlanTrans($arrPlanTrans);
        if($arrMenuandSubmitPlan != []){
            $objPromocode = new Promocode();
            $arrPromocode = $objPromocode->getPromocodebyId($arrMenuandSubmitPlan[0]["promocode_id"]);
            $arr['arr_menu'] = json_decode($arrMenuandSubmitPlan[0]['menu']);
            if($arr['arr_menu'] != null){
                $arr['arr_menu'] = $arr['arr_menu']->data;
            }
        }else{
            $arrPromocode = null;
            $arr['arr_menu'] = null;
        }
        $arr['user'] = $User;
        $arr['user_address'] = $arrUserAddress;
        $arr['user_q'] = $resultQuestionnaire;
        $arr['plan_ass'] = $arrMenuandSubmitPlan;
        $arr['promocode'] = $arrPromocode;
        $arr['freeze_plan'] = $resultFreezePlan;
        // dd($arr['arr_menu']);
        return view('profile', compact('arr'));
    }

}
