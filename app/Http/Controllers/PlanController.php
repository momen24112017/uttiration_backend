<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserBuyPlan;
use App\Mail\ReportAdmin;
use App\AddressUser;
use App\PromocodeTransaction;
use App\PlanTransaction;
use Illuminate\Support\Carbon;
use App\Status;
use App\Promocode;

class PlanController extends Controller
{
    public function BuyPlan(Request $request){
        $arr = array();
        $input = $request->all();
        
        if(isset($input['promocode_id'])){
            $promocodeTransactionInput = array(
                'user_id'      => $input['user_id'],
                'promocode_id' => $input['promocode_id'] 
            );
            $stroePromocodeTransaction = PromocodeTransaction::create($promocodeTransactionInput);
        }

        $objPlanTransaction = new PlanTransaction();
        $arrPlanTrans = $objPlanTransaction->checkUserPlan($input['user_id']);
        foreach($arrPlanTrans as $obj){
            if($input['from'] <= $obj['to']){
                $arr['sign date'] = $input['from'];
                $arr['plan date'] = $obj['to'];
                $arr = Status::mergeStatus($arr,5016);
                return $arr;        
            }
        }
        
        $input['from'] = Carbon::parse($input['from'])->format('Y-m-d'); 
        $input['to'] = Carbon::parse($input['to'])->format('Y-m-d');
        
        $result = PlanTransaction::create($input);
        $objPromocode = new Promocode();
        $promoValue = 0;
        if(isset($input['promocode_id'])){
        $promocode= $objPromocode->getPromocodebyId($input['promocode_id']);
        $promoValue = $promocode['value'];
        }
        $objPlan = new Plan();
        $Plan = $objPlan->getPlaneById($input['plan_id']);
        $objUser = new User();
        $user=$objUser->getUserById($input['user_id']);
        $data = array(
            'name'    => $user['name'],
            'email'   => $user['email'],
            'mobile' => $user['phone'],
            'meal_name'=> $Plan['name'],
            'price'=>$input['price'],
            'promo_value'=>$promoValue,
            'img'=>env('App_Media_URL').$Plan['feature_img']
            
        ); 
        // $resultEmail = Mail::to($data['email'])->send(new UserBuyPlan($data));
        // $adminEmail = Mail::to('mirette.mahmoud@rethinktechs.com')->send(new ReportAdmin($data));

        $arr['data'] = $result;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListPlan(){
        $arr = array();
        $objPlan = new Plan();
        $result = $objPlan->listPlane();
        $arr['data'] = $result;
        $arr['color'] = ['first','second','third'];
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

}
