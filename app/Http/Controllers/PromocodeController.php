<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PromocodeTransaction;
use App\Promocode;
use Illuminate\Support\Carbon;
use App\Status;

class PromocodeController extends Controller
{

    public function CheckPromocode($promocode){
        $arr = array();
        $objUser = new Promocode();
        $result = $objUser->checkPromocode($promocode);
        if($result == false){
            $arr['data'] = $result;
            $arr = Status::mergeStatus($arr,5010);
            return $arr;
        }
        $now = now();
        foreach($result as $key=>$value){
            $dateValue = $value['valid_date'];
        }
        if($dateValue < $now){
            $arr['data'] = $result;
            $arr = Status::mergeStatus($arr,5010);
            return $arr;
        }
        $value = $objUser->promocodeValue($promocode);
        $arr['promocode'] = $objUser->promocode($promocode);
        $arr['data'] = $value;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    } 

}
