<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanTransaction extends Model
{
    protected $fillable = [
        'plan_id', 'user_id', 'promocode_id', 'price', 'menu', 'from', 'to'
    ];

    public function checkUserPlan($user_id){
        $arrPlanTrans = $this->where('user_id',$user_id)->orderBy('id', 'desc')->take(2)->get();
        return $arrPlanTrans;
    }

    public function getUserPlanTrans($user_id){
        $dateNowMY = date('Y-m-d');
        $arrAssignedPlan = array();
        $arrPlanTrans = $this->where('user_id',$user_id)->orderBy('id', 'desc')->take(2)->get();
        foreach($arrPlanTrans as $obj){
            if($dateNowMY < $obj['to']){
                $arrAssignedPlan[] = $obj;
            }
        }
        return $arrAssignedPlan;
    }

    public function storeUserPlan($input, $id){
        $userMenu = PlanTransaction::where('user_id', $id)->where('menu', null)->latest()->update(array('menu' => json_encode($input)));
        return $userMenu;
    }

    public function addSubmitMenu($path, $id){
        $update = PlanTransaction::where('user_id', $id)->where('file', null)->latest()->update(['file'=> $path]);
        return $update;
    }

}
