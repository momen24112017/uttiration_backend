<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuUser extends Model
{
    protected $fillable = [
        'user_id', 'menu', 'file'
    ];

    public function handleMenuUserTable($userId, $file){
        $emptyColumn = MenuUser::where('menu', '[]')->delete();
        $objMenu = MenuUser::orderBy('id', 'desc')->first();
        $dateColumn = MenuUser::where('id', $objMenu['id'])->update(array('user_id'=> $userId,'file'=> $file));
    }

    public function getMenuPlanAttachedtoPlanTrans($arrPlanTrans){
        foreach($arrPlanTrans as $key=>$obj){
            if($obj['menu'] != null){  
                $arrPlanTrans[$key]['menuUser'] = array();
                $arrWeek1 = array();
                $y=1;
                $arrM = json_decode($obj['menu']);
                foreach($arrM as $objMenu){
                    if($objMenu->day =="thursday"){
                        $arrWeek1['week'.$y][] = $objMenu;
                        $y++;
                    }else if($objMenu->day == "friday"){
                        $y--;
                        $arrWeek1['week'.$y][] = $objMenu;
                        $y++;
                        
                    }else{
                        $arrWeek1['week'.$y][] = $objMenu;
                    }
                }
                $arrPlanTrans[$key]['menuUser']  =  $arrWeek1;
            }
            
            if($obj['menu'] == ''||$obj['menu']==null){
                $datefrom = date('Y-m', strtotime($obj['from']));
                $objMenu = $this->where('user_id',$obj['user_id'])->orderBy('id', 'DESC')->first();
                if($objMenu != null){
                    $arrPlanTrans[$key]['menu_submit'] = array();
                    $arrMenu = json_decode($objMenu->menu);
                    $arrWeek = array();
                    $i=1;
                    foreach($arrMenu as $objMenu){
                        if($objMenu->day =="thursday"){
                            $arrWeek['week'.$i][] = $objMenu;
                            $i++;
                        }else if($objMenu->day == "friday"){
                            $i--;
                            $arrWeek1['week'.$i][] = $objMenu;
                            $i++;
                            
                        }else{
                            $arrWeek['week'.$i][] = $objMenu;
                        }
                    }
                    $arrPlanTrans[$key]['menu_submit'] = $arrWeek;
                }
            }
        }
        return $arrPlanTrans;
    }
}
